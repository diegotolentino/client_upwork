# To Install
``
pipenv sync
``

# Dependencies and explanations
splinter: Simulate a real user in the site, due to scrap prevention tecniques
that the upwork use

beautifulsoup4: Easy to parse html docs 

# To Run
``
pipenv shell
python upwork.py --username USER --password PASS --secret SECRET

or

pipenv run python upwork.py --username USER --password PASS --secret SECRET
``

# To Do list

- TBD: Return more info about the job, clicking in every one
- Unit tests
- Smarter load detection / remove use of sleep
- More error resilience 
