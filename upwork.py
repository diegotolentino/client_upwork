import argparse
import re
import time

from bs4 import BeautifulSoup as bs
from splinter import Browser


class Upwork:
    def __init__(self):
        self.driver = Browser()
        self.is_logged = False
        self.login_url = "https://www.upwork.com/ab/account-security/login"
        self.profile_url = 'https://www.upwork.com/freelancers/settings/contactInfo'

    def __del__(self):
        self.driver.quit()

    def login(self, username, password, secret=None):
        self.username = username
        self.password = password
        self.secret = secret

        # Visit URL
        self.driver.visit(self.login_url)

        field = self.driver.find_by_id('login_username')
        field.click()
        field.fill(username)

        # Find and click the 'Continue' button
        button = self.driver.find_by_id('login_password_continue')
        button.click()

        # Check the elements
        if not self.driver.is_text_present('Welcome'):
            raise Exception("Something wrong")

        time.sleep(3)
        field = self.driver.find_by_id('login_password')
        field.click()
        field.fill(password)

        # Find and click the 'Log in' button
        button = self.driver.find_by_id('login_control_continue')
        button.click()

        # check and pass device auth
        self._device_auth()

        self.is_logged = True
        return True

    def _get_session_items(self):
        job_items = self.driver.find_by_css('div#feed-jobs > section')
        result = []
        for item in job_items:
            result.append(item.html)

        return result

    def _device_auth(self):
        if self.driver.is_text_present('Device authorization'):
            field = self.driver.find_by_id('deviceAuth_answer')
            field.click()
            field.fill(self.secret)

            # Find and click the 'Authorize' button
            button = self.driver.find_by_id('control_save')
            button.click()

            time.sleep(3)

        if self.driver.is_text_present('Re-enter password'):
            self.driver.find_by_css('input[type="password"]')[0].fill(self.password)

            button = self.driver.find_by_id('control_continue')
            button.click()

            time.sleep(3)

    @staticmethod
    def _parse_item(html_content):
        result = {}
        soup = bs(html_content, 'html.parser')

        result['title'] = soup.select('h4.job-title')[0].text.strip()

        # type and sometimes budget price
        content = soup.find_all('span', attrs={'data-job-type': True})[0].text.strip()
        if '$' in content:
            content, budget = content.split(':')
            result['budget'] = budget.strip()

        result['type'] = content

        # tier
        result['tier'] = soup.find_all('span', attrs={'data-job-tier': True})[0].text.replace('-', '').strip()

        # duration
        el = soup.find_all('span', attrs={'data-job-duration': True})[0]
        if el.text.strip():
            result['duration'] = el.span.text.split(':')[1].strip()

        # budget
        el = soup.find_all('span', attrs={'data-job-budget': True})[0]
        if el.text.strip():
            result['budget'] = el.span.text.split(':')[1].strip()

        # publish date
        el = soup.find_all('span', attrs={'data-job-posted-time': True})[0]
        result['published'] = el.find('time').attrs['datetime']

        # proposals
        el = soup.find_all('span', attrs={'data-job-proposals': True})[0]
        result['proposals'] = el.text.split(':')[1].strip()

        # description
        el = soup.find_all('div', attrs={'data-job-description': True})[0]
        result['description'] = el.find('span', attrs={'data-ng-bind-html': 'htmlToTruncate'}).text.strip()

        # client location
        el = soup.find_all('span', attrs={'data-job-client-location': True})[0]
        result['client-location'] = el.text.strip()

        # skills
        el = soup.select('div.skills')
        if el:
            result['skills'] = [s.text.strip() for s in el[0].find_all('span') if 'data-ng-repeat' in s.attrs]

        return result

    def get_jobs(self):
        result = {}
        for menu in self.driver.find_by_css('ul.nav-pills li'):
            menu.click()
            result[menu.text] = []
            time.sleep(3)
            for item in self._get_session_items():
                item_parsed = self._parse_item(item)
                result[menu.text].append(item_parsed)

        return result

    def get_profile_info(self):

        result = {
            'id': None,
            'account': self.username,
            'employer': None,
            'full_name': None,
            'first_name': None,
            'last_name': None,
            'email': None,
            'phone_number': None,
            'birth_date': None,
            'picture_url': None,
            'address.line1': None,
            'address.line2': None,
            'address.city': None,
            'address.state': None,
            'address.postal_code': None,
            'address.country': None,
            'ssn': None,
            'marital_status': None,
            'gender': None,
        }

        self.driver.visit(self.profile_url)

        self._device_auth()

        # open edit of location and contact
        self.driver.find_by_text('Edit')[1].find_by_xpath(".//ancestor::button").click()
        self.driver.find_by_text('Edit')[0].find_by_xpath(".//ancestor::button").click()
        time.sleep(3)
        soup = bs(self.driver.html, 'html.parser')

        # select simple items
        result['id'] = self.driver.evaluate_script('Applet.getUser().getUid()')
        result['email'] = self.driver.find_by_name('email').value
        result['address.city'] = self.driver.find_by_name('city').value
        result['address.postal_code'] = self.driver.find_by_name('zip').value
        result['address.line1'] = self.driver.find_by_name('street')[0].value
        result['address.line2'] = self.driver.find_by_name('additionalInfo')[0].value
        result['picture_url'] = soup.select('img.nav-avatar')[0].attrs['src']

        # select first + last name
        first = self.driver.find_by_css('#firstName').value
        last = self.driver.find_by_css('#lastName').value
        result['first_name'] = first
        result['last_name'] = last
        result['full_name'] = first + ' ' + last

        # selecting country
        el = soup.find('label', text='Country')
        result['address.country'] = el.parent.find_next('div').select('button > span')[0].text

        # select state
        el = soup.find('label', text='State')
        result['address.state'] = el.parent.select('button > span')[0].text

        # select phone country + number
        el = soup.find('label', text='Phone')
        code = el.parent.select('button > span')[0].text
        code = re.sub('[^0-9+]', '', code)
        number = self.driver.find_by_name('number').value
        result['phone_number'] = code + ' ' + number

        return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upwork crawler')
    parser.add_argument('--username', required=True)
    parser.add_argument('--password', required=True)
    parser.add_argument('--secret')
    args = parser.parse_args()

    crawler = Upwork()
    crawler.login(username=args.username, password=args.password,
                  secret=args.secret)
    print(crawler.get_jobs())
    print(crawler.get_profile_info())
